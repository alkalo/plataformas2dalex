﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public GameObject options;

	

	public void Empezar(){
		SceneManager.LoadScene ("StageFour");
	}
	public void MainMen(){
		SceneManager.LoadScene ("MainMenu");
	}
	public void Credits(){
		SceneManager.LoadScene ("Credits");
	}
	public void Exit(){
		Application.Quit();
	}
	public void OptionsON(){
		options.SetActive(true);
	}
	public void OptionsOFF(){
		options.SetActive(false);
	}
	public void FullScreen(){
		Screen.fullScreen = !Screen.fullScreen;
	}



}
