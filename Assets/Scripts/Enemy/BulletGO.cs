﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletGO : MonoBehaviour {

    private float speed = 5.0f;
    private Vector3 velocity;
	
	// Update is called once per frame
	void Update () {
        transform.position += velocity * Time.deltaTime * speed;
        transform.Translate(-speed*Time.deltaTime,0,0);
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Rebote")
        {
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "invokedMateria")
        {

        }
    }
}
