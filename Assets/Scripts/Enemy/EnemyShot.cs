﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShot : MonoBehaviour {
    private bool canshot = true;
    public GameObject bulletprefab;
    public float timer = 1f;
    public Transform cannon;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Shot();
            timer = 1f;
        }
    }

    void Shot() {
        Instantiate(bulletprefab,cannon.position,Quaternion.identity);
    }
}
