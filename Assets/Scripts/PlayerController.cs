﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : PhysicsObject
{
    public float maxSpeed = 5;
    public float jumpTakeOffSpeed = 10;
	public GameController gm;

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    private bool flipX;

    public Transform m_currMovingPlatform { get; private set; }

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    // Use this for initialization
    void Awake()
    {
        
        
       animator = GetComponent<Animator>();
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;
        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded)
        {
            velocity.y = jumpTakeOffSpeed;
			animator.SetBool ("Ground", true);
        }

		else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }
			

        if (Input.GetKeyDown(KeyCode.A))
        {
            spriteRenderer.flipX = true;
        }else if (Input.GetKeyDown(KeyCode.D))
        {
            spriteRenderer.flipX = false;
        }

        if (velocity.y <= 0)
        {
			animator.SetBool("Ground", false);
            if (move.x > 0 || move.x < 0)
            {
				animator.SetBool("Run", true);
            }
            else
            {
				animator.SetBool("Run", false);
            }
        }

        targetVelocity = move * maxSpeed;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Coin") {
            gm.coins += 1;
            Destroy(collision.gameObject);
        }

        if (collision.transform.tag == "Dead" || collision.transform.tag == "Enemy")
        {

            Application.LoadLevel(Application.loadedLevel);
        }
		if(collision.tag == "win"){
			SceneManager.LoadScene ("MainMenu");
		}
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Dead" || collision.transform.tag == "Enemy")
        {

            Application.LoadLevel(Application.loadedLevel);
        }
    }

}