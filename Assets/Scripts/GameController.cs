﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    //TimerVARS===================================
    public Text tiempoText;
    public float tiempo = 0.0f;
    //TimerVARS===================================

    //CoinsVARS===================================
    public Text coinsText;
    public int coins = 0;
    //CoinsVARS===================================


   // Update is called once per frame
    void Update()
    {

        //Timer ============================================
        if (tiempoText != null && tiempo > 0)
        {
            tiempo -= Time.deltaTime;
            tiempoText.text = "" + tiempo.ToString("f0");
        }
        else if (tiempo == 0)
        {
            tiempo = 0;
        }

        if (tiempo <= 0) {
            Application.LoadLevel(Application.loadedLevel);
        }
        //Timer ============================================


        //Coins ===================================
        if (coinsText != null)
        {
            coinsText.text = "" + coins.ToString("f0");
        }
			
    }




}
